/*
 * File: StoneMasonKarel.java
 * --------------------------
 * The StoneMasonKarel subclass as it appears here does nothing.
 * When you finish writing it, it should solve the "repair the quad"
 * problem from Assignment 1.  In addition to editing the program,
 * you should be sure to edit this comment so that it no longer
 * indicates that the program does nothing.
 */

import stanford.karel.*;

public class StoneMasonKarel extends SuperKarel {
//Methodendeklaration
	private void putBeepersUntilColumnEnd() {
		while (frontIsClear()){			
			if (noBeepersPresent()) {		
				putBeeper();
				}
			move();
		}
		if (noBeepersPresent()) {			
			putBeeper();
			}
	}
	
	private void buildColumn() {
		while (notFacingNorth()) {
			turnLeft();
		}
		putBeepersUntilColumnEnd();
		moveDownColumn();
	}
		
	private void moveDownColumn() {
		while(notFacingSouth()) {
			turnLeft();
		}
		while (frontIsClear()) {
			move();
		}
		while(notFacingEast()) {
			turnLeft();
		}		
	}
	
	
	//MainMethode
	private void runToNextColumn() {		
		move();
		move();
		move();
		move();
	}	
	public void run() {		
while (frontIsClear()) {
	buildColumn();
	runToNextColumn();
		}
		buildColumn();
	}
	

}
